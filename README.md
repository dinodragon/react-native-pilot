### Get Started ###

This is just a pilot project to kick-off react-native. Nothing impressive.. :P

1. Clone this repo into your local machine
2. Open Terminal in MAC and go to the directory where you clone the repo
3. Type `npm install` in Terminal and click enter
4. Type `react-native run-ios` in Terminal and click enter

The **reactive-native client** and **iphone simulator** should have run after complete the above steps

### Star Dust ###
This project is using `React-Native` with `mobX`. Enjoy~~