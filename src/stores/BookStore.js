import {observable, action, computed, autorun} from 'mobx';

const REQUEST_URL = 'https://www.googleapis.com/books/v1/volumes?q=';

class BookStore{
	@observable books = []

	@action getFeaturedBooks(){
    let self = this;

		return fetch(REQUEST_URL + 'subject:fiction')
      .then((response) => response.json())
      .then((responseData) => { self.books = responseData.items; });
	}

  @action searchBooks(bookTitle, bookAuthor){
    let self = this;
    let query = [];

    if (bookAuthor !== '')
      query.push(encodeURIComponent('inauthor:' + bookAuthor));

    if (bookTitle !== '') 
      query.push(encodeURIComponent('intitle:' + bookTitle));

    let queryString = query.join('+');
    return fetch(REQUEST_URL + queryString)
      .then(response => response.json())
      .then(responseData => {
        self.books = responseData.items || []
      })
      .catch(error => {});
  }
}

export default new BookStore;
