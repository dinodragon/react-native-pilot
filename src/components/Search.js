import React, {Component} from 'react';
import {StyleSheet, View, Text, NavigatorIOS, Navigator} from 'react-native';
import SearchBook from './SearchBook'

const styles = StyleSheet.create({
  description: {
    fontSize: 20,
    backgroundColor: 'white'
  },
  container: {
    flex: 1
  }
});
 
export default class Search extends Component {
  render() {
    return (
      <NavigatorIOS
        style={styles.container}
        initialRoute={{title: 'Search Book', component: SearchBook}}
      />
    );
  }
}

// NAvigator example: not using this whiel folowing the tutorial
// <Navigator
//   style={styles.container}
//   initialRoute={{title: 'Search Books'}}
//   renderScene={(route, nav) => <SearchBook />}
//   navigationBar={
//      <Navigator.NavigationBar
//        routeMapper={{
//          LeftButton: (route, navigator, index, navState) => { return null; },
//          RightButton: (route, navigator, index, navState) => { return null; },
//          Title: (route, navigator, index, navState) => { return (<Text>Awesome Nav Bar</Text>); }
//        }}
//      />
//   }
// />