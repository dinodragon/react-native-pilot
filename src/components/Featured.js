import React, {Component} from 'react';
import {StyleSheet, View, Text, NavigatorIOS} from 'react-native';

import BookList from './BookList'

const styles = StyleSheet.create({
  description: {
    fontSize: 20,
    backgroundColor: 'white'
  },
  container: {
    flex: 1
  }
});
 
export default class Featured extends Component {
  render() {
    return (
      <NavigatorIOS
        style={styles.container}
        initialRoute={{title: 'Featured Books', component: BookList}}
      />    
    );
  }
}